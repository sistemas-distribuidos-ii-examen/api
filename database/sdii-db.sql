DROP SCHEMA IF EXISTS sdii-db CASCADE;
CREATE SCHEMA sdii-db;

------------------------------------------------ TABLAS ------------------------------------------------

CREATE TABLE "public"."sdii_ciudadano" (
    "id" integer DEFAULT nextval('sdii_ciudadano_id_seq') NOT NULL,
    "cuil" character varying(45) NOT NULL,
    "nombre" character varying(45) NOT NULL,
    "apellido" character varying(45) NOT NULL,
    "nro_doc" character varying(45) NOT NULL,
    "nro_tramite" character varying(45) NOT NULL,
    "telefono" character varying(45),
    "email" character varying(45),
    "sexo" character varying(45),
    CONSTRAINT "sdii_ciudadano_cuil_key" UNIQUE ("cuil"),
    CONSTRAINT "sdii_ciudadano_pkey" PRIMARY KEY ("id")
);

CREATE TABLE "public"."sdii_domicilio" (
    "id" integer DEFAULT nextval('sdii_domicilio_id_seq') NOT NULL,
    "id_provincia" character varying(45),
    "id_departamento" character varying(45),
    "id_localidad" character varying(45),
    "domicilio" character varying(45),
    "lat_localidad" character varying(45),
    "long_localidad" character varying(45),
    CONSTRAINT "sdii_domicilio_pkey" PRIMARY KEY ("id")
);

CREATE TABLE "public"."sdii_permiso" (
    "cod_permiso" integer DEFAULT nextval('sdii_permiso_cod_permiso_seq') NOT NULL,
    "fecha_vigencia" timestamptz NOT NULL,
    "ciudadano_id" integer NOT NULL,
    "domicilio_id" integer NOT NULL,
    CONSTRAINT "sdii_permiso_pkey" PRIMARY KEY ("cod_permiso")
);

--Autodefinidas
