# Install aglio

npm install -g aglio

# Aglio comandos
aglio -i documentacion_api.apib -o documentacion.html
aglio -i documentacion_api.apib --server

#Ejecucion en primer plano
docker-compose -f deployment.yaml up

#Ejecucion en segundo plano
docker-compose -f deployment.yaml up -d

# Git
git clone url.gitlab.com

# Heroku

# PASOS PARA CORRER LUEGO DE HACER CAMBIOS

heroku login
git add --all
git commit -m "cambio_descripcion"
git push heroku master 


# Docker comandos generales
docker ps // procesos
docker logs <id_contenedor>
docker stop <id_contenedor> <id_contenedor>

# PASOS PARA EJECUTAR TODO EN LOCAL
1) "estar parado en la carpeta inicial, en nestro caso 'api' "
2) cd django-api
2) sudo docker build -t sdii-api:1.0 .
3) cd ../
4) sudo docker-compose -f deployment.yaml up -d