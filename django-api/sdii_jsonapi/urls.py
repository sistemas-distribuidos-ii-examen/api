from django.urls import include, path
from rest_framework import routers
from sdii import views

from django.conf import settings
from django.conf.urls.static import static

router = routers.DefaultRouter()
router.register(r'ciudadanos', views.CiudadanoViewSet)
router.register(r'domicilios', views.DomicilioViewSet)
router.register(r'permisos', views.PermisoViewSet)

urlpatterns = [
    path('', include(router.urls)),
] 
