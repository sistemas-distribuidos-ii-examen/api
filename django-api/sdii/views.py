from django.shortcuts import render

# Create your views here.

from sdii.models import Ciudadano, Domicilio, Permiso
from sdii.serializers import CiudadanoSerializer,PermisoSerializer,DomicilioSerializer
from rest_framework import viewsets

class CiudadanoViewSet(viewsets.ModelViewSet):
    queryset = Ciudadano.objects.all()
    serializer_class = CiudadanoSerializer

class PermisoViewSet(viewsets.ModelViewSet):
    queryset = Permiso.objects.all()
    serializer_class = PermisoSerializer

class DomicilioViewSet(viewsets.ModelViewSet):
    queryset = Domicilio.objects.all()
    serializer_class = DomicilioSerializer