from django.db import models

class Ciudadano(models.Model):
    cuil = models.CharField(max_length=45, unique=True)
    nombre = models.CharField(max_length=45)
    apellido = models.CharField(max_length=45)
    nro_doc = models.CharField(max_length=45)
    nro_tramite = models.CharField(max_length=45)
    telefono = models.CharField(max_length=45,null=True)
    email = models.CharField(max_length=45,null=True)
    sexo = models.CharField(max_length=45,null=True)
    
class Domicilio(models.Model):
    id_provincia = models.CharField(max_length=45,null=True)
    id_departamento = models.CharField(max_length=45,null=True)
    id_localidad = models.CharField(max_length=45,null=True)
    domicilio = models.CharField(max_length=45,null=True)
    lat_localidad = models.CharField(max_length=45,null=True)
    long_localidad = models.CharField(max_length=45,null=True)

class Permiso(models.Model):
    cod_permiso = models.AutoField(primary_key=True)
    fecha_vigencia = models.DateTimeField(blank=True)
    domicilio = models.ForeignKey(Domicilio, on_delete=models.CASCADE)
    ciudadano = models.ForeignKey(Ciudadano, on_delete=models.CASCADE)


