from rest_framework_json_api import serializers
from sdii.models import Domicilio, Permiso, Ciudadano

class PermisoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model=Permiso
        fields = ('fecha_vigencia','cod_permiso','domicilio','ciudadano')

class DomicilioSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model=Domicilio
        fields = ('id_provincia','id_departamento','id_localidad','domicilio','lat_localidad','long_localidad','permiso_set')

class CiudadanoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model=Ciudadano
        fields = ('cuil','nombre','apellido','nro_doc','nro_tramite','telefono','email','sexo','permiso_set')

